# Weather app

#### Description:
The project is frontend for api provided by OpenWeatherMap. You can check the weather in all countries around the world.
#### Technologies used:
* javascript, 
* ES6
* sass, 
* html, 
* fetch,
* webpack

#### How to run:
1. Download repo
2. In main directory type: _npm_ _install_
3. Type: _npm_ _run_ _dev_

<img src="https://drive.google.com/uc?export=view&id=1hKWq1fh1-_9YWhJp00El6FUiiW5iSANm" width="400" height="auto" align="left">
<img src="https://drive.google.com/uc?export=view&id=1RDGJj5qfQ6V0aI2ubPoPhYmj76cTa55v" width="200" height="auto" align="left">