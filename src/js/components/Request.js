export default class Request {
  constructor(
    url = 'http://api.openweathermap.org/data/2.5/weather',
    city = 'London',
    units = 'metric',
    api_key = '059cf3d51733fc79e1108464a4cd9b90'){
    this.api_key = api_key;
    this.city = city;
    this.units = units;
    this.url = url;
    this.init();
  }
  getWeather(){
    fetch(`${this.url}?q=${this.city}&appid=${this.api_key}&units=${this.units}`)
      .then(response => response.json())
      .then(response => console.log(response))
  }

  init(){
    this.getWeather();
  }

}